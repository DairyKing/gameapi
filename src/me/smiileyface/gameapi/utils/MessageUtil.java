package me.smiileyface.gameapi.utils;

import org.apache.commons.lang.Validate;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class MessageUtil {

	public static String chatBar;
	
	static {
		StringBuilder chatBarBuilder = new StringBuilder();
		for(int x = 0; x < 34; x++) {
			chatBarBuilder.append(MessageCharacter.BOX_LINE.toString());
		}
		chatBar = chatBarBuilder.toString();
	}
	
	public static void newLine(CommandSender sender) {
		sender.sendMessage("");
	}
	
	public static void send(CommandSender sender, String... messages) {
		Validate.notNull(messages, "Messages is null");
		
		for(String message : messages) {
			sender.sendMessage(StringUtil.colorize(message));
		}
	}
	
	public static void sendAll(String... messages) {
        Validate.notNull(messages, "Messages is null");

        for (Player player : Bukkit.getOnlinePlayers()) {
            send(player, messages);
        }
    }

    public static void sendBordered(CommandSender sender, BorderedMessageType type, String... messages) {
        Validate.notNull(messages, "Messages is null");

        send(sender, type.getBorderColor() + MessageCharacter.BOX_TOP_LEFT_CORNER.toString() + chatBar);
        send(sender, type.getBorderColor() + MessageCharacter.BOX_SIDE.toString() + "  " + type);
        send(sender, type.getBorderColor() + MessageCharacter.BOX_SIDE.toString());
        for (String message : messages) {
            send(sender, type.getBorderColor() + MessageCharacter.BOX_SIDE.toString() + "  &7" + message);
        }
        send(sender, type.getBorderColor() + MessageCharacter.BOX_SIDE.toString());
    }

    public static void sendPrefix(CommandSender sender, String message) {
        send(sender, StringUtil.getPrefixedString(message));
    }

    public static void broadcast(String... messages) {
        Validate.notNull(messages, "Messages is null");

        for (String message : messages) {
            Bukkit.broadcastMessage(StringUtil.colorize(message));
        }
    }

    public static void broadcastPrefix(String message) {
        Validate.notNull(message, "Message is null");

        broadcast(StringUtil.getPrefixedString(message));
    }
	
	public static enum BorderedMessageType {
		SEVERE_WARNING(ChatColor.RED, ChatColor.DARK_RED, "Severe Warning"),
		WARNING(ChatColor.YELLOW, ChatColor.GOLD, "Warning"),
		NOTIFICATION(ChatColor.AQUA, ChatColor.DARK_AQUA, "Notification"),
		GAMEAPI(ChatColor.GOLD, ChatColor.DARK_GRAY, "GameAPI");
		
		private ChatColor titleColor, borderColor;
		private String title;
		
		BorderedMessageType(ChatColor titleColor, ChatColor borderColor, String title) {
			this.titleColor = titleColor;
			this.borderColor = borderColor;
			this.title = title;
		}
		
		public ChatColor getTitleColor() {
			return titleColor;
		}
		
		public ChatColor getBorderColor() {
			return borderColor;
		}
		
		public String getTitle() {
			return title;
		}
		
		@Override
		public String toString() {
			return titleColor.toString() + ChatColor.BOLD.toString() + title;
		}
	}
	
	public static enum MessageCharacter {
		RAQUO('\u00bb'),
		LAQUO('\u00ab'),
        DOT('\u25CF'),
        BOX_TOP_LEFT_CORNER('\u2554'),
        BOX_BOTTOM_LEFT_CORNER('\u255a'),
        BOX_LINE('\u2550'),
        BOX_SIDE('\u2551');

        private char c;

        MessageCharacter(char c) {
            this.c = c;
        }

        public char getC() {
            return c;
        }

        @Override
        public String toString() {
            return Character.toString(c);
        }
	}
}
