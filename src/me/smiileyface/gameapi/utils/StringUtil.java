package me.smiileyface.gameapi.utils;

import org.apache.commons.lang.StringUtils;
import org.bukkit.ChatColor;
import org.bukkit.util.ChatPaginator;

public class StringUtil {

	private static char colorCodeTranslate = '&';
    private static final String PREFIX = colorize("&6&lMALENITE&r&8: &7");

    public static char getColorCodeChar() {
        return colorCodeTranslate;
    }

    public static String getPrefix() {
        return PREFIX;
    }

    public static String getPrefixedString(String string) {
        return PREFIX + StringUtil.colorize(string);
    }

    public static void setColorCodeChar(char colorCodeChar) {
        colorCodeTranslate = colorCodeChar;
    }

    public static String colorize(String string) {
        return ChatColor.translateAlternateColorCodes(colorCodeTranslate, string);
    }

    public static String stripColors(String canStrip) {
        for (int i = 0; i < canStrip.length(); i++) {
            if (canStrip.charAt(i) == '&') {
                canStrip = canStrip.replaceFirst("&" + canStrip.charAt(i + 1), "");
            }
        }
        return canStrip;
    }

    public static String center(String text) {
        return StringUtils.center(text, ChatPaginator.AVERAGE_CHAT_PAGE_WIDTH);
    }

    public static boolean isInt(String toTest) {
        try {
            Integer.parseInt(toTest);
        } catch (NumberFormatException ex) {
            return false;
        }
        return true;
    }

    public static boolean isDouble(String toTest) {
        try {
            Double.parseDouble(toTest);
        } catch (NumberFormatException ex) {
            return false;
        }
        return true;
    }

    public static int toInteger(String string) throws NumberFormatException {
        try {
            return Integer.parseInt(string.replaceAll("[^\\d]", ""));
        } catch (NumberFormatException e) {
            throw new NumberFormatException(string + " isn't a number!");
        }
    }

    public static double toDouble(String string) {
        try {
            return Double.parseDouble(string.replaceAll(".*?([\\d.]+).*", "$1"));
        } catch (NumberFormatException e) {
            throw new NumberFormatException(string + " isn't a number!");
        }
    }
}
