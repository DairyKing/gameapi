package me.smiileyface.gameapi.kit;

import me.smiileyface.gameapi.game.GameManager;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;


public abstract class Kit implements Listener {

	public GameManager manager;
	private String name;
	private String[] desc;
	private Material displayItem;
	
	public Kit(GameManager manager, String name, Material displayItem, String[] desc) {
		this.manager = manager;
		
		this.name = name;
		this.desc = desc;
		
		this.displayItem = displayItem;
	}
	
	public String getName() {
		return name;
	}
	
	public String[] getDescription() {
		return desc;
	}
	
	public Material getDisplayItem() {
		return displayItem;
	}
	
	public void applyKit(Player player) {
		player.getInventory().clear();
		player.getInventory().setArmorContents(null);
		
		giveItems(player);
		
		player.updateInventory();
	}
	
	public abstract void giveItems(Player player);
}
