package me.smiileyface.gameapi.arena;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import me.smiileyface.gameapi.Main;
import me.smiileyface.gameapi.utils.ZipUtil;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.entity.Creature;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.entity.LivingEntity;

public class Arena {

	private String name;
	private String creator;
	private World world;
	private List<ArenaSpawn> spawns = new ArrayList<>();
	private Location center;
	
	public Arena(String name, String creator, Location center, List<Location> spawn) {
		this.name = name;
		this.creator = creator;
		this.center = center;
		for(Location l : spawn) {
			spawns.add(new ArenaSpawn(l, spawns.size()+1));
		}
	}
	
	public void loadWorld() {
		ZipUtil.extractZIP(new File(Main.get().getDataFolder() + "/maps/" + getName() + ".zip"), new File(getName()));
		world = Bukkit.createWorld(new WorldCreator(getName().toLowerCase()));
		world.setMonsterSpawnLimit(0);
		world.setAnimalSpawnLimit(0);
		world.setSpawnFlags(false, true);
		world.setTicksPerAnimalSpawns(0);
		world.setTicksPerMonsterSpawns(0);
		for(Entity e : world.getEntities()) {
			if(e instanceof Item) {
				e.remove();
				continue;
			}
			if(!(e instanceof LivingEntity))
				continue;
			if((LivingEntity)e instanceof Creature) {
				e.remove();
			}
		}
		
		for(ArenaSpawn aSpawn : spawns) {
			aSpawn.getSpawn().getChunk().load();
		}
		world.setStorm(false);
	}
	
	public String getName() {
		return name;
	}
	
	public String getCreator() {
		return creator;
	}
	
	public World getWorld() {
		if(Bukkit.getWorld(getName().toLowerCase()) == null) {
			loadWorld();
		}
		return world;
	}
	
	public Location getCenter() {
		return center;
	}
	
	public List<ArenaSpawn> getSpawns() {
		return spawns;
	}
	
	public Location getSpawn(boolean unoccupied) {
		for(ArenaSpawn aSpawn : spawns) {
			if(unoccupied && !aSpawn.isOccupied()) {
				return aSpawn.getSpawn();
			} else {
				return aSpawn.getSpawn();
			}
		}
		return null;
	}
	
	public Location getRandomSpawn() {
		int r = new Random().nextInt(spawns.size());
		return spawns.get(r).getSpawn();
	}
}
