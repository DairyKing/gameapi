package me.smiileyface.gameapi.arena;

import java.util.ArrayList;
import java.util.List;

import me.smiileyface.gameapi.Main;
import me.smiileyface.gameapi.utils.LocationUtil;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;

public class ArenaManager {

	public static final ArenaManager INSTANCE = new ArenaManager();
	
	private List<Arena> arenas = new ArrayList<>();
	
	public List<Arena> getArenas() {
		return arenas;
	}
	
	public void addArena(Arena a) {
		arenas.add(a);
	}
	
	public Arena getArenaByName(String name) {
		for(Arena a : arenas) {
			if(a.getName().equalsIgnoreCase(name)) {
				return a;
			}
		}
		return null;
	}
	
	public void loadArenas() {
		FileConfiguration config = Main.get().getConfig();
		for(String arena : config.getConfigurationSection("arenas").getKeys(false)) {
			ArrayList<Location> spawns = new ArrayList<>();
			for(String spawn : config.getConfigurationSection("arenas." + arena + ".spawns").getKeys(false)) {
				spawns.add(LocationUtil.strToLoc(Bukkit.getWorld(config.getString("arenas." + arena + ".name")), spawn));
			}
			Arena a = new Arena(config.getString("arenas." + arena + ".name"), config.getString("arenas." + arena + ".creator"), LocationUtil.strToLoc(Bukkit.getWorld(config.getString("arenas." + arena + ".name")), config.getString("arenas." + arena + ".center")), spawns);
			addArena(a);
		}
	}
}
