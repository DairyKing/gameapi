package me.smiileyface.gameapi.arena;

import org.bukkit.Location;

public class ArenaSpawn {

	private int id;
	private Location location;
	private boolean occupied = false;
	
	public ArenaSpawn(Location location, int id) {
		this.location = location;
		this.id = id;
	}
	
	public Location getSpawn() {
		return location;
	}
	
	public boolean isOccupied() {
		return occupied;
	}
	
	public void setOccupied(boolean val) {
		this.occupied = val;
	}
	
	public int getId() {
		return id;
	}
}
