package me.smiileyface.gameapi.team;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import me.smiileyface.gameapi.utils.StringUtil;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class Team {

	private String name;
	private ChatColor color;
	private List<UUID> players = new ArrayList<UUID>();
	
	public Team(String name, ChatColor color) {
		this.name = name;
		this.color = color;
	}
	
	public String getName() {
		return name;
	}
	
	public ChatColor getColor() {
		return color;
	}
	
	public List<Player> getOnlinePlayers() {
		List<Player> players = new ArrayList<>();
		for(UUID uuid : this.players) {
			Player player = Bukkit.getPlayer(uuid);
			if(player != null) {
				players.add(player);
			}
		}
		return players;
	}
	
	public List<UUID> getUUIDPlayers() {
		return players;
	}
	
	public boolean hasPlayer(UUID player) {
		return players.contains(player);
	}
	
	public void addPlayer(UUID player) {
		if(!hasPlayer(player)) {
			players.add(player);
		}
	}
	
	public void removePlayer(UUID player) {
		if(hasPlayer(player)) {
			players.remove(player);
		}
	}
	
	public void clear() {
		players.clear();
	}
	
	public void broadcastMessage(String msg) {
		for(Player player : getOnlinePlayers()) {
			player.sendMessage(StringUtil.colorize(msg));
		}
	}
}
