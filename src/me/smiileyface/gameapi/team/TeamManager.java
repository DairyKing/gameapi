package me.smiileyface.gameapi.team;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.ChatColor;

public class TeamManager {

	public static final TeamManager INSTANCE = new TeamManager();
	
	private List<Team> teams = new ArrayList<>();
	
	public Team getTeam(UUID player) {
		for(Team team : teams) {
			if(team.hasPlayer(player)) {
				return team;
			}
		}
		return null;
	}
	
	public Team getTeamByName(String name) {
		for(Team team : teams) {
			if(team.getName().equalsIgnoreCase(name)) {
				return team;
			}
		}
		return null;
	}
	
	public Team getTeamByColor(ChatColor color) {
		for(Team team : teams) {
			if(team.getColor().equals(color)) {
				return team;
			}
		}
		return null;
	}
	
	public List<Team> getTeams() {
		return teams;
	}
	
	public void addTeam(Team team) {
		if(!teams.contains(team)) {
			teams.add(team);
		}
	}
	
	public void removeTeam(Team team) {
		if(teams.contains(team)) {
			teams.remove(team);
		}
	}
	
	public void removeAllTeams() {
		teams.forEach(Team::clear);
	}
	
	
}
