package me.smiileyface.gameapi.events;

import me.smiileyface.gameapi.game.Game;
import me.smiileyface.gameapi.game.GameState;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class GameStateChangeEvent extends Event {

	public static final HandlerList handlers = new HandlerList();
	private Game game;
	private GameState state;
	
	public GameStateChangeEvent(GameState gameState, Game game) {
		this.state = gameState;
		this.game = game;
	}
	
	public GameState getState() {
		return state;
	}
	
	public Game getGame() {
		return game;
	}
	
	@Override
	public HandlerList getHandlers() {
		return handlers;
	}
	
	public static HandlerList getHandlerList() {
		return handlers;
	}
}
