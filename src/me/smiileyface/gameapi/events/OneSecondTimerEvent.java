package me.smiileyface.gameapi.events;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class OneSecondTimerEvent extends Event {

	public static final HandlerList handlers = new HandlerList();
	
	@Override
	public HandlerList getHandlers() {
		return handlers;
	}
	
	public static HandlerList getHandlerList() {
		return handlers;
	}
}
