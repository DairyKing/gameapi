package me.smiileyface.gameapi;

import me.smiileyface.damageapi.DamageLib;
import me.smiileyface.gameapi.events.OneSecondTimerEvent;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandExecutor;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {

	private static Main instance;
	public static Main get() {
		return instance;
	}
	
	@Override
	public void onEnable() {
		instance = this;
		
		new DamageLib();
		
		Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
			
			@Override
			public void run() {
				OneSecondTimerEvent e = new OneSecondTimerEvent();
				Bukkit.getServer().getPluginManager().callEvent(e);
			}
		}, 20L, 20L);
	}
	
	public void registerListener(Listener listener) {
		getServer().getPluginManager().registerEvents(listener, this);
	}
	
	public void registerCommand(String label, CommandExecutor executor) {
		getCommand(label).setExecutor(executor);
	}
}
