package me.smiileyface.gameapi.game;

public enum GameState {

	LOBBY, PREPARE, LIVE, END, DEAD;
}
