package me.smiileyface.gameapi.game;

import java.util.HashSet;

import org.bukkit.block.Block;
import org.bukkit.entity.Item;

public class GameManager {

	public Game game;
	
	public String name;
	public boolean beta;
	
	public boolean damage = true;
	public boolean damagePvP = true;
	public boolean damagePvE = true;
	public boolean damageEvP = true;
	public boolean damageSelf = true;
	public boolean damageTeamSelf = false;
	public boolean damageTeamOther = true;
	
	public boolean blockBreak = false;
	public HashSet<Block> blockBreakAllow = new HashSet<>();
	
	public boolean blockPlace = false;
	public HashSet<Block> blockPlaceAllow = new HashSet<>();
	
	public boolean itemPickup = false;
	public HashSet<Item> itemPickupAllow = new HashSet<>();
	
	public boolean itemDrop = false;
	public HashSet<Item> itemDropAllow = new HashSet<>();
	
	public boolean deathOut = true;
	public boolean deathDropItems = false;
	
	public boolean creatureAllow = false;
	public boolean creatureAllowOverride = false;
	
	public boolean firstKill = true;
	
	public GameManager(Game game, String name, boolean beta) {
		this.game = game;
		this.name = name;
		this.beta = beta;
	}
	
	public Game getGame() {
		return game;
	}
	
	public String getName() {
		return name;
	}
	
	public boolean inBeta() {
		return beta;
	}
}
