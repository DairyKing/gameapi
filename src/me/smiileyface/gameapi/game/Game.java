package me.smiileyface.gameapi.game;

import me.smiileyface.gameapi.arena.Arena;
import me.smiileyface.gameapi.events.GameStateChangeEvent;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

public abstract class Game implements Listener {

	public GameManager manager;
	private String[] desc;
	
	private GameState state = GameState.LOBBY;
	private long gameStateTime = System.currentTimeMillis();
	
	private boolean prepareCountdown = false;
	
	private int countdown = -1;
	private boolean countdownForce = false;
	
	private Arena arena = null;
	
	public Game(GameManager manager, String[] desc) {
		this.manager = manager;
		this.desc = desc;
	}
	
	public String[] getDescription() {
		return desc;
	}
	
	public GameState getState() {
		return state;
	}
	
	public void setState(GameState state) {
		this.state = state;
		this.gameStateTime = System.currentTimeMillis();
		
		GameStateChangeEvent e = new GameStateChangeEvent(state, this);
		Bukkit.getServer().getPluginManager().callEvent(e);
	}
	
	public long getStateTime() {
		return gameStateTime;
	}
	
	public boolean inProgress() {
		return (getState() == GameState.PREPARE) || (getState() == GameState.LIVE);
	}
	
	public boolean isLive() {
		return getState() == GameState.LIVE;
	}
	
	public void setSpectator(Player player) {
		player.getInventory().clear();
		player.getInventory().setArmorContents(null);
		player.getActivePotionEffects().clear();
		
		player.teleport(arena.getCenter());
		player.setGameMode(GameMode.ADVENTURE);
		player.setFlying(true);
		player.setFlySpeed(0.1F);
		
		for(Player other : Bukkit.getOnlinePlayers()) {
			if(other == player)
				continue;
			other.hidePlayer(player);
		}
	}
	
	public void startGame() {
		
	}
	
	public void endGame() {
		
	}
	
	public abstract void checkEnd();
	
	public void startPrepareCountdown() {
		this.prepareCountdown = true;
	}
	
	public boolean canStartPrepareCountdown() {
		return this.prepareCountdown;
	}
}
