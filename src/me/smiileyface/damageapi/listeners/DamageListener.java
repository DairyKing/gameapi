package me.smiileyface.damageapi.listeners;

import me.smiileyface.damageapi.damage.BlockDamageTick;
import me.smiileyface.damageapi.damage.DamageManager;
import me.smiileyface.damageapi.damage.DamageTick;
import me.smiileyface.damageapi.damage.FallDamageTick;
import me.smiileyface.damageapi.damage.MonsterDamageTick;
import me.smiileyface.damageapi.damage.OtherDamageTick;
import me.smiileyface.damageapi.damage.PlayerDamageTick;
import me.smiileyface.damageapi.damage.TNTDamageTick;
import me.smiileyface.damageapi.events.EntityDamagedByPlayerTNTEvent;

import org.apache.commons.lang.WordUtils;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByBlockEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;

public class DamageListener implements Listener {

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onDamage(EntityDamageEvent e) {
		
		Entity entity = e.getEntity();
		if(!(entity instanceof LivingEntity)) {
			return;
		}
		
		DamageTick item = null;
		double dmg = e.getFinalDamage();
		
		if(e.isCancelled() || dmg == 0)return;
		
		if((e instanceof EntityDamageByEntityEvent)) {
			EntityDamageByEntityEvent evt = (EntityDamageByEntityEvent)e;
			
			Entity damager = evt.getDamager();
			
			LivingEntity attacker = null;
			double distance = 0;
			
			if((damager instanceof LivingEntity)) {
				attacker = (LivingEntity)damager;
			} else if((damager instanceof Projectile)) {
				Projectile projectile = (Projectile)damager;
				if((projectile.getShooter() instanceof LivingEntity)) {
					attacker = (LivingEntity)projectile.getShooter();
					distance = attacker.getLocation().distance(entity.getLocation());
				}
			} else if((damager instanceof TNTPrimed)) {
				TNTPrimed tnt = (TNTPrimed) damager;
				if(tnt.getSource() != null && ((tnt.getSource() instanceof Player)) && !tnt.getSource().getUniqueId().equals(entity.getUniqueId())) {
					EntityDamagedByPlayerTNTEvent event = new EntityDamagedByPlayerTNTEvent((Player) tnt.getSource(), entity, tnt, dmg);
					Bukkit.getPluginManager().callEvent(event);
					if(event.isCancelled()) {
						e.setCancelled(true);
						return;
					}
					item = new TNTDamageTick(dmg, "TNT", System.currentTimeMillis(), (Player) tnt.getSource(), tnt.getLocation());
				} else {
					item = new BlockDamageTick(dmg, EntityDamageEvent.DamageCause.ENTITY_EXPLOSION, "TNT", System.currentTimeMillis(), Material.TNT, damager.getLocation());
				}
			}
			
			if(attacker != null) {
				if((attacker instanceof Player)) {
					if(distance > 0) {
						item = new PlayerDamageTick(dmg, "PVP", System.currentTimeMillis(), (Player)attacker, distance);
					} else {
						 item = new PlayerDamageTick(dmg, "PVP", System.currentTimeMillis(), (Player)attacker);
                    }
				} else {
					if (distance > 0){
                        item = new MonsterDamageTick(dmg, "PVE", System.currentTimeMillis(), attacker, distance);
                    } else {
                        item = new MonsterDamageTick(dmg, "PVE", System.currentTimeMillis(), attacker);
                    }
				}
			}
		} else if((e instanceof EntityDamageByBlockEvent)) {
			EntityDamageByBlockEvent evt = (EntityDamageByBlockEvent)e;
            Block block = evt.getDamager();
            if (block != null && block.getType() != Material.AIR) {
                item = new BlockDamageTick(dmg, EntityDamageEvent.DamageCause.BLOCK_EXPLOSION, "BLOCK", System.currentTimeMillis(), block.getType(), block.getLocation());
            } else {
                item = getOtherTick(e, dmg);
            }
        }
        else{

            if (e.getCause() == EntityDamageEvent.DamageCause.FALL){
                item = new FallDamageTick(dmg, "Fall", System.currentTimeMillis(), entity.getFallDistance());
            }
            else{
                item = getOtherTick(e, dmg);
            }

        }

        if (item != null){
            DamageManager.logTick(entity.getUniqueId(), item);
        }
	}
	
	private OtherDamageTick getOtherTick(EntityDamageEvent event, double dmg) {
        EntityDamageEvent.DamageCause cause = event.getCause();
        if (cause == EntityDamageEvent.DamageCause.FIRE_TICK) {
            cause = EntityDamageEvent.DamageCause.FIRE;
        }

        String name = cause.name();
        name = WordUtils.capitalizeFully(name).replace("_", " ");

        return new OtherDamageTick(dmg, cause, name, System.currentTimeMillis());
    }
}
