package me.smiileyface.damageapi;

import me.smiileyface.damageapi.listeners.DamageListener;
import me.smiileyface.damageapi.listeners.DeathListener;
import me.smiileyface.gameapi.Main;

import org.bukkit.ChatColor;

public class DamageLib {

	public static final long DAMAGE_TIMEOUT = 5000;
	public static final int ASSIST_PERCENTAGE_THRESHOLD = 20;
	public static final ChatColor BASE_COLOR = ChatColor.GRAY;
	public static final ChatColor ACCENT_COLOR = ChatColor.DARK_RED;
	public static final ChatColor PUNCTUATION_COLOR = ChatColor.DARK_GRAY;
	
	public DamageLib() {
		Main.get().registerListener(new DamageListener());
		Main.get().registerListener(new DeathListener());
	}
}
