package me.smiileyface.damageapi.damage;

import java.text.DecimalFormat;

import me.smiileyface.damageapi.DamageLib;

import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent;

public class FallDamageTick extends DamageTick {

	private double distance;

    public FallDamageTick(double damage, String name, long timestamp, double distance) {
        super(damage, EntityDamageEvent.DamageCause.FALL, name, timestamp);
        this.distance = distance;
    }

    public double getDistance() {
        return distance;
    }

    @Override
    public boolean matches(DamageTick tick) {
        return false;
    }

    @Override
    public String getDeathMessage(Player player) {
        DecimalFormat df = new DecimalFormat("#");
        return DamageLib.ACCENT_COLOR + player.getDisplayName() + DamageLib.BASE_COLOR + " was killed by " + DamageLib.ACCENT_COLOR + df.format(getDistance()) + DamageLib.BASE_COLOR + " block fall";
    }

    @Override
    public String getSingleLineSummary() {
        DecimalFormat df = new DecimalFormat("#");
        return DamageLib.BASE_COLOR + "Fell " + DamageLib.ACCENT_COLOR + df.format(getDistance()) + DamageLib.BASE_COLOR + " blocks";
    }
}
